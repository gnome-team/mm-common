Source: mm-common
Section: devel
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Jeremy Bícha <jbicha@ubuntu.com>, Michael Biebl <biebl@debian.org>, Sebastian Dröge <slomo@debian.org>
Build-Depends: debhelper-compat (= 13),
               meson (>= 0.50.0),
Standards-Version: 4.6.2
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/gnome-team/mm-common
Vcs-Git: https://salsa.debian.org/gnome-team/mm-common.git
Homepage: https://www.gtkmm.org/

Package: mm-common
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         automake | automaken,
         libtool,
         pkg-config
Recommends: g++ | c++-compiler,
            doxygen,
            graphviz,
            xsltproc
Suggests: libglibmm-2.4-dev,
          libsigc++-2.0-doc,
          libglibmm-2.4-doc
Description: Common build files of the GNOME C++ bindings
 The mm-common module provides the build infrastructure and utilities
 shared among the GNOME C++ binding libraries.  It is only a required
 dependency for building the C++ bindings from the gnome.org version
 control repository.  An installation of mm-common is not required for
 building tarball releases, unless configured to use maintainer-mode.
 .
 The mm-common package also includes a snapshot of the Doxygen tag file
 for the GNU C++ Library reference documentation.
